# enable-disable-service-docker-compose

Objectives:
- enable/disable service
- use .env file

### Steps

- copy .env.xxx to .env, modify parameters in .env
- run 
  ```
  docker-compose up --build
  ```
- view log to inspect service-1-1 is started or not
